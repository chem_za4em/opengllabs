
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#define TO_RAD 0.01745329


#define SEGMENT 128

float windowWidth  = 700.0;
float windowHeight = 700.0;
float angle = 0.0;

GLfloat x = 0;
GLfloat y = 0;
GLfloat r = 3;


void drawPlanet(float x_center, float y_center, float radius, int RED, int GREEN, int BLUE){

    glBegin(GL_TRIANGLE_FAN);
    glColor3ub(RED, GREEN, BLUE);
    glVertex2f(x_center, y_center);
    for(int i = 0; i<=SEGMENT; i++){

        float angle = (float)i / (float)SEGMENT * 3.1415 * 2.0;
        glVertex2f(x_center - cos(angle) * radius, y_center- sin(angle)*radius);

    }
    glEnd();

}

void drawLine(float x_first, float y_first, float x_second, float y_second, int RED, int GREEN, int BLUE){

    glBegin(GL_LINES);
    glColor3ub(RED, GREEN, BLUE);
    glVertex2d(x_first, y_first);
    glVertex2d(x_second, y_second);
    glEnd();

}

void drawStar(int x_center, int y_center){

    drawLine(x_center-5, y_center,   x_center+5, y_center,   255, 255, 255);
    drawLine(x_center,   y_center-5, x_center,   y_center+5, 255, 255, 255);
    drawLine(x_center-4, y_center-4, x_center+4, y_center+4, 255, 255, 255);
    drawLine(x_center-4, y_center+4, x_center+4, y_center-4, 255, 255, 255);

}

void drawHouse(float angle){

    GLfloat bodyHouse[12] = {
        -13, 65, 0.0,
        -15, 95, 0.0,
        15, 95, 0.0,
        13, 65, 0.0 };

    GLfloat windowHouse[12] = {
        -9, 75, 0.0,
        -10, 90, 0.0,
        10, 90, 0.0,
        9, 75, 0.0 };

    GLfloat barsHouse[18] = {
        0, 75,  0.0,
        0, 90,  0.0,
        0, 84,  0.0,
        10, 84,  0.0,};

    GLfloat roofHouse[9] = {
        -20,  95, 0.0,
        0, 118, 0.0 ,
        20,  95, 0.0 };


    glEnableClientState(GL_VERTEX_ARRAY);

    glColor3ub(240, 220, 130);
    glVertexPointer(3, GL_FLOAT, 0, bodyHouse);
    glDrawArrays(GL_POLYGON, 0, 4);


    if((angle>=0.0&&angle<=90.0)||(angle>=180.0&&angle<=270)){
        glColor3ub(255, 255, 0);
    }else if(angle>=90.0&&angle<=180.0){
        glColor3ub(255, 255, 255);
    }else {
        glColor3ub(0, 0, 0);
    }
    glVertexPointer(3, GL_FLOAT, 0, windowHouse);
    glDrawArrays(GL_POLYGON, 0, 4);

    glColor3ub(0, 0, 0);
    glVertexPointer(3, GL_FLOAT, 0, barsHouse);
    glDrawArrays(GL_LINES, 0, 4);

    glColor3ub(150, 75, 0);
    glVertexPointer(3, GL_FLOAT, 0, roofHouse);
    glDrawArrays(GL_POLYGON, 0, 3);


    glDisableClientState(GL_VERTEX_ARRAY);

}

void rotateHouse(){

    glPushMatrix();
    glTranslatef(380.0, 380.0, 0.0);
    glRotatef(angle, 0.0, 0.0, 1.0);

    drawHouse(angle);

    glTranslatef(-380.0,-380.0, 0.0);
    glLoadIdentity();
    glPopMatrix();

}


void timer(int value){

    angle+=0.9;

    if(angle == 360.0){

        angle = 0.0;

    }

    glutPostRedisplay();
    glutTimerFunc(30, timer, 0);

}

void initialize(){

    glClearColor(0, 0, 0, 0);

}

void display(){

    glClear(GL_COLOR_BUFFER_BIT);


    rotateHouse();

    drawPlanet(380, 380, 70,  0,   100, 255);       //земля
    drawPlanet(540, 540, 20,  127, 127, 127);      //луна
    drawPlanet(-99, -99, 400, 255, 255, 0);       //солнце

    drawStar(30,  500);
    drawStar(240, 400);
    drawStar(350, 680);
    drawStar(400, 600);
    drawStar(580, 100);
    drawStar(600, 300);
/*
    glEnable(GL_LINE_SMOOTH);
    glEnable(GL_LINE_STIPPLE);
    glLineStipple(1,255);
    glBegin(GL_LINES);
    glColor3ub(255, 255, 255);
    glVertex2f(0, 0);
    glVertex2f(380, 380);
    glEnd();

    glDisable(GL_LINE_STIPPLE);
    glDisable(GL_LINE_SMOOTH);
*/

    glFlush();
    glFinish();

}

void changeSize(int width, int height){

    glViewport(0, 0, windowWidth, windowHeight);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, windowWidth, 0, windowHeight);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

}

int main(int argc, char **argv){
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE);
    glutInitWindowSize(windowWidth, windowHeight);
    glutCreateWindow("My universe");
    initialize();
    glutDisplayFunc(display);
    glutTimerFunc(30, timer, 0);
    glutReshapeFunc(changeSize);

    glutMainLoop();
    return 0;

}
