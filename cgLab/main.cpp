#include "GL/glut.h"
#include "math.h"
#include <fstream>

#define RADIAN 0.01745329

using namespace std;

GLuint textures[9];

GLfloat heightWindow = 700;
GLfloat widthWindow = 700;
GLfloat depthWindow = 700;

GLfloat radiusSphere = 70;
GLfloat segmentSphere = 10;
GLfloat corner = 250;         //длина ребра октаэдра
GLfloat space = 20;       //отступ ребра от координат
GLfloat azimutAngle = 23;     //азимутный угол обзора сцены
GLfloat zenitAngle = 23;      //зенитный угол обзора сцены
GLfloat orbit_eye = 150;       //дальность камеры

    //направление камеры
GLfloat xEye = 1;
GLfloat yEye = 1;
GLfloat zEye = 0;

GLfloat angleOctahedron = 0;     //угол вращения октаэдра
GLfloat angleSphere = 10;     //угол вращения сферы
GLfloat radiusOrbit = 350;      //радиус вращения сферы

    //координаты сферы
GLfloat xSphere = 0;
GLfloat ySphere = 0;
GLfloat zSphere = 0;

GLfloat octahedronSpeed = 1; //скорость вращения октаэдра
GLfloat sphereSpeed = 1;     //скорость вращения сферы

GLfloat position[3] = { xSphere, ySphere, zSphere };         //откуда светит свет
GLfloat direction[3]= { xSphere-1, ySphere-1, zSphere-1 };   //куда направлен свет(вектор)

GLfloat fogColor[4] = { 0.3, 0.2, 0.1, 0.1 };    //цвет тумана
GLfloat whiteColor[3] = { 1, 1, 1 };             //белый цвет
GLfloat blackColor[3] = { 0, 0, 0 };             //черный цвет

GLfloat diffuseSphere[3] = { 0.5, 0.5, 0.5 };  //яркость света
GLfloat background[3] = { 0.3, 0.3, 0.3 };     //фоновый цвет

GLfloat square = 0;    //инициализация диспл списка
GLint lengthCell = 110;   //длина ячейки
GLint countCell = 19;     //количество ячеек

GLfloat textureVertexArray[48] = {

    0.0, 0.2,	0.9,  0,    1, 0.3,
    0.0, 0.2,	0.9,  0,    1, 0.3,
    0.0, 0.2,	0.9,  0,    1, 0.3,
    0.0, 0.2,	0.9,  0,    1, 0.3,
    0.0, 0.2,	0.9,  0,    1, 0.3,
    0.0, 0.2,	0.9,  0,    1, 0.3,
    0.0, 0.2,	0.9,  0,    1, 0.3,
    0.0, 0.2,	0.9,  0,    1, 0.3,

};   //координаты текстур на полигоне
GLfloat vertexArray[72] = {

    -corner,  space,  space,    -space,  corner,  space,    -space,  space,  corner,
     corner,  space,  space,     space,  corner,  space,     space,  space,  corner,
     corner,  space, -space,     space,  corner, -space,     space,  space, -corner,
    -corner,  space, -space,    -space,  corner, -space,    -space,  space, -corner,
    -corner, -space,  space,    -space, -corner,  space,    -space, -space,  corner,
     corner, -space,  space,     space, -corner,  space,     space, -space,  corner,
     corner, -space, -space,     space, -corner, -space,     space, -space, -corner,
    -corner, -space, -space,    -space, -corner, -space,    -space, -space, -corner,

};         //массив вершин
GLfloat normals[72] = {

     1, -1, -1,     1, -1, -1,     1, -1, -1,
     1,  1,  1,     1,  1,  1,     1,  1,  1,
    -1, -1,  1,    -1, -1,  1,    -1, -1,  1,
    -1,  1, -1,    -1,  1, -1,    -1,  1, -1,
    -1, -1,  1,    -1, -1,  1,    -1, -1,  1,
    -1,  1, -1,    -1,  1, -1,    -1,  1, -1,
     1, -1, -1,     1, -1, -1,     1, -1, -1,
     1,  1,  1,     1,  1,  1,     1,  1,  1,

};        //массив нормалей
GLfloat colorArray[72] = {

    1.0,   0.0,   0.0,      1.0,   0.0,   0.0,      1.0,   0.0,   0.0,
    1.0,   0.4,   0.0,      1.0,   0.4,   0.0,      1.0,   0.4,   0.0,
    1.0,   1.0,   0.0,      1.0,   1.0,   0.0,      1.0,   1.0,   0.0,
    0.0,   1.0,   0.0,      0.0,   1.0,   0.0,      0.0,   1.0,   0.0,
    0.0,   0.0,   0.8,      0.0,   0.6,   0.8,      0.0,   0.6,   0.8,
    0.0,   0.0,   1.0,      0.0,   0.0,   1.0,      0.0,   0.0,   1.0,
    0.5,   0.0,   0.5,      0.5,   0.0,   0.5,      0.5,   0.0,   0.5,
    1.0,   0.0,   0.0,      0.0,   1.0,   0.0,      0.0,   0.0,   1.0

};        //массив цветов
GLfloat whiteArray[72] = {

    1, 1, 1,   1, 1, 1,	  1, 1, 1,
    1, 1, 1,   1, 1, 1,	  1, 1, 1,
    1, 1, 1,   1, 1, 1,	  1, 1, 1,
    1, 1, 1,   1, 1, 1,	  1, 1, 1,
    1, 1, 1,   1, 1, 1,	  1, 1, 1,
    1, 1, 1,   1, 1, 1,	  1, 1, 1,
    1, 1, 1,   1, 1, 1,	  1, 1, 1,
    1, 1, 1,   1, 1, 1,   1, 1, 1

};       //белая цвет для норм отображения текстур

GLboolean octDirMoveFlag = false;  //направление вращения октаэдра
GLboolean sphDirMoveFlag = false;      //направление вращения сферы
GLboolean octahedronMoveFlag = false;           //флаг движения октаэдра
GLboolean textureCountFlag = false;          //флаг количества текстур
GLboolean chessboardFlag = false;               //флаг включенности дисплейных списков
GLboolean sphereMoveFlag = false;        //флаг движения сферы
GLboolean textureFlag = false;                  //флаг включенности текстур
GLboolean blendFlag = false;                    //флаг прозрачности
GLboolean fogFlag = false;               //флаг тумана
GLboolean lightFlag = true;                     //флаг освещения



void blend(void){

    if (blendFlag)
    {
        glDisable(GL_DEPTH_TEST);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    }

}

void camera(void){

    glLoadIdentity();
        xEye = orbit_eye*sinf(zenitAngle*RADIAN);
        yEye = orbit_eye*sinf(azimutAngle*RADIAN);
        zEye = orbit_eye*cosf(zenitAngle*RADIAN);
    gluLookAt(xEye, yEye, zEye, 0, 0, 0, 0, 1, 0);    // откуда, куда, угол

}

void loadTexture(const char *nameTexture){

    GLchar *data;
    FILE *texture;
    texture  = fopen(nameTexture, "rb");

    data = (char *)malloc(128*128*9);

    fread(data, 1, 128*128, texture);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);   // горизонтальная
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);   // вертикалная координата
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);   //фильтрация,когда изображение на экране больше, чем текстура используется средневзвешенное значение по пикселю 2x2, ближайших к центру.
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);     // параметры текстуризации - Коды цветов текселей умножаются на коды цветов геометрических фрагментов.
        gluBuild2DMipmaps(GL_TEXTURE_2D, 4, 128, 128, GL_BGR_EXT, GL_UNSIGNED_BYTE, data);  // кол-во компонентов цвета, тип хранения, размер

    fclose(texture);
    free(data);

}

void drawQuads(void){

    for(int x = 0; x<countCell*lengthCell; x+=lengthCell){

        for(int y = 0; y<countCell*lengthCell; y+=lengthCell){
            if(((x+y)/lengthCell)%2==0)
            {

                glBegin(GL_QUADS);
                glColor3ub(255, 255, 255);
                glVertex2f(x, y);
                glVertex2f(x, y+lengthCell);
                glVertex2f(x+lengthCell, y+lengthCell);
                glVertex2f(x+lengthCell, y);
                glEnd();

            }
        }
    }

}


void initChessboard(void){

    square = glGenLists(1);
    GLfloat modul = lengthCell*countCell/2;

    glNewList(square, GL_COMPILE);

        glPushMatrix();
            glTranslatef(-modul, -modul, modul);
            drawQuads();
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-modul, -modul, -modul);
            drawQuads();
        glPopMatrix();

        glPushMatrix();
            glTranslatef(-modul, modul, -modul);
            glRotated(180, 1, 0, 1);
            drawQuads();
        glPopMatrix();

        glPushMatrix();
            glTranslatef(modul, modul, -modul);
            glRotated(180, 1, 0, 1);
            drawQuads();
        glPopMatrix();

        glPushMatrix();
            glTranslatef(modul, modul, -modul);
            glRotated(180, 0, 1, 1);
            drawQuads();
        glPopMatrix();

        glPushMatrix();
            glTranslatef(modul, -modul, -modul);
            glRotated(180, 0, 1, 1);
            drawQuads();
        glPopMatrix();


    glEndList();

}



void drawChessboard(void){


    if(chessboardFlag){
        if(square == 0)
        initChessboard();
        glCallList(square);

    }

}

void drawLight(void){
    if(lightFlag){
        xSphere = radiusOrbit;

        glLightModelfv(GL_LIGHT_MODEL_AMBIENT, background); //фон

        glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseSphere);
        glLightfv(GL_LIGHT0, GL_POSITION, position);        //прожектор
        glEnable(GL_LIGHT0);
    } else {
        glDisable(GL_LIGHT0);
    }

}

void drawOctahedron(void){

    glEnable(GL_TEXTURE_2D);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    for (int i = 0; i < 8; i++){

        glVertexPointer(3, GL_FLOAT, 0, vertexArray);
        glNormalPointer(GL_FLOAT, 0, normals);

        if (textureFlag){

            glTexCoordPointer(2, GL_FLOAT, 0, textureVertexArray);
            glColorPointer(3, GL_FLOAT, 0, whiteArray);

            if (textureCountFlag){

                glBindTexture(GL_TEXTURE_2D, textures[0]);

            } else {

                glBindTexture(GL_TEXTURE_2D, textures[i]);
            }

        } else {

            glDisable(GL_TEXTURE_2D);
            glColorPointer(3, GL_FLOAT, 0, colorArray);

        }

        glDrawArrays(GL_TRIANGLES, 3 * i, 3);

    }

    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisable(GL_TEXTURE_2D);

}

void rotateOctahedron(void){

    glPushMatrix();
    glRotatef(angleOctahedron, 0, 1, 0);

        drawOctahedron();

    glLoadIdentity();
    glPopMatrix();

    if(octahedronMoveFlag){
        if(octDirMoveFlag){
            angleOctahedron+=octahedronSpeed;
        } else {
            angleOctahedron-=octahedronSpeed;
        }
    } else {
        angleOctahedron = angleOctahedron;
    }

}

void drawSphere(GLint radius){

    glColor3ub(255, 255, 255);
    glutWireSphere(radius, segmentSphere, segmentSphere);
//    glutSolidSphere(radius, segmentSphere, segmentSphere);

}

void rotateSphere(void){

    glPushMatrix();

        glRotatef(angleSphere, 0, 1, 0);
        glTranslatef(-radiusOrbit, 0, 0);

        drawLight();
        drawSphere(radiusSphere);

    glLoadIdentity();
    glPopMatrix();

    if(sphereMoveFlag){
        if(sphDirMoveFlag){
            angleSphere+=sphereSpeed;
        } else {
            angleSphere-=sphereSpeed;
        }
    } else {
        angleSphere = angleSphere;
    }

}


void display(void){

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
       glEnable(GL_LIGHTING);          //включаем работу со светом

        camera();
        drawChessboard();

        rotateSphere();
        rotateOctahedron();

    glFlush();
    glDisable(GL_LIGHTING);          //выключаем работу со светом
    glutSwapBuffers();

}

void reshape(GLint width, GLint height){

    glMatrixMode(GL_PROJECTION);  //переключение матрицы

    glLoadIdentity();       //загрузка матрицы

    glViewport(-170, -150,  widthWindow+350, heightWindow+350);

    glOrtho(-widthWindow, widthWindow, -heightWindow, heightWindow, -depthWindow, depthWindow*10);  //сцена
    glMatrixMode(GL_MODELVIEW);  // переключаем матрицу обратно


}

void keyboard(unsigned char key, GLint x, GLint y){


    switch(key){

        case 's':            //поворот камеры вверх
            azimutAngle -= 2;
            break;

        case 'w':            //поворот камеры вниз
            azimutAngle += 1;
            break;

        case 'a':            //поворот камеры направо
            zenitAngle += 1;
            break;

        case 'd':            //поворот камеры налево
            zenitAngle -= 2;
            break;

        case '1':
            octahedronMoveFlag = true;
            if(octahedronMoveFlag)
                octDirMoveFlag = !octDirMoveFlag;
            break;

        case '2':
            sphereMoveFlag = true;
            if(sphereMoveFlag)
                sphDirMoveFlag = !sphDirMoveFlag;
            break;

        case '3':
        case 'b':
            octahedronMoveFlag = false;
                sphereMoveFlag = false;
            break;

        case 'u':
            textureFlag = !textureFlag;
            break;

        case 'i':
            textureCountFlag = !textureCountFlag;
            break;

        case 'p':
            chessboardFlag = !chessboardFlag;
            break;

        case '[':
            if (blendFlag){
                blendFlag = !blendFlag;
                glDisable(GL_BLEND);
                glEnable(GL_DEPTH_TEST);
            } else {
                blendFlag = !blendFlag;
                glEnable(GL_BLEND);
            }
            blend();
            break;

        case 'o':
            if (fogFlag){
                fogFlag = !fogFlag;
                glDisable(GL_FOG);
            } else{
                fogFlag = !fogFlag;
                glEnable(GL_FOG);
            }
            break;

        case 'q':
            lengthCell -= 5;
            break;

        case 'e':
            lengthCell += 5;
            break;

        case 'k':
            countCell+=1;
            break;

        case 'l':
            countCell-=1;
            break;

        case 'j':
            lightFlag = !lightFlag;
            break;


        }

}

void initialize(void){

    glClearColor(0, 0, 0, 1);


    glEnable(GL_NORMALIZE);         //нормирует нормаль(единичная)
    glEnable(GL_DEPTH_TEST);        //включает ось z(буффер глубины)
    glEnable(GL_COLOR_MATERIAL);    //закрашивает полигончики своими цветами
    glEnable(GL_POLYGON_SMOOTH);    //сглаживает полигоны

    glShadeModel(GL_BLEND);        // прозрачность
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);  //для картинок
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);   //  с обеих старон заполненный многоугольник
    glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);  // определяет будет ли освещение расчитываться только для внешних граней или и для внешних и для внутренних



    glGenTextures(8, textures);
        glBindTexture(GL_TEXTURE_2D, textures[0]);          // индексация текстур
            loadTexture("/home/chem/Документы/cg/2.bmp");
        glBindTexture(GL_TEXTURE_2D, textures[1]);
            loadTexture("/home/chem/Загрузки/app/3.bmp");
        glBindTexture(GL_TEXTURE_2D, textures[2]);
            loadTexture("/home/chem/Загрузки/app/0.bmp");
        glBindTexture(GL_TEXTURE_2D, textures[3]);
            loadTexture("/home/chem/Загрузки/app/0.bmp");
        glBindTexture(GL_TEXTURE_2D, textures[4]);
            loadTexture("/home/chem/Загрузки/app/0.bmp");
        glBindTexture(GL_TEXTURE_2D, textures[5]);
            loadTexture("/home/chem/Загрузки/app/0.bmp");
        glBindTexture(GL_TEXTURE_2D, textures[6]);
            loadTexture("/home/chem/Загрузки/app/0.bmp");
        glBindTexture(GL_TEXTURE_2D, textures[7]);
            loadTexture("/home/chem/Загрузки/app/0.bmp");


    glFogfv(GL_FOG_COLOR, fogColor);  //установка цвета тумана
        glHint(GL_FOG_HINT, GL_NICEST);   //доп установка
  //      glFogi(GL_FOG_MODE, GL_LINEAR);    // тип тумана
  //      glFogi(GL_FOG_MODE, GL_EXP2);    // тип тумана
        glFogi(GL_FOG_MODE, GL_EXP);    // Обычный туман, заполняющий весь экран
        glFogf(GL_FOG_DENSITY, 0.005f);    // густота тумана
        glFogf(GL_FOG_START, 0);        // Глубина, с которой начинается туман
        glFogf(GL_FOG_END, 100);        // Глубина, где туман заканчивается.

}

void timer(GLint value){

    glutPostRedisplay();
    glutTimerFunc(10, timer, 0); // 3ий параметр, если использ неск таймеров

}

int main(GLint argc, GLchar **argv){

    //openManual();

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(widthWindow, heightWindow);
    glutCreateWindow("CG");

    initialize();

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutTimerFunc(100, timer, 0);


    glutMainLoop();   // бесконечный цикл, чтобы функция не завершалась

    return 1;

}
